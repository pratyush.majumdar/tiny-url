package com.example.pratyush.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.pratyush.service.TinyURL;

@Controller
public class IndexController {
	@Autowired
	private TinyURL tinyUrl;
	
	private String longURL;
	private String tinyURL;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showWelcomePage() {
		
		return "index";
	}
	
	@RequestMapping(value = "/input", method = RequestMethod.POST)
	public String getLongUrl(@RequestParam String longUrl) {		
		this.longURL = longUrl;
		tinyURL = tinyUrl.getTinyURL(longUrl);
		
		return "redirect:/tiny-url";
	}
	
	@RequestMapping(value = "/tiny-url", method = RequestMethod.GET)
	public String showTinyURL(ModelMap model) {
		model.addAttribute("long_url", longURL);
		model.addAttribute("tiny_url", tinyURL);
		return "tiny-url";
	}
}
