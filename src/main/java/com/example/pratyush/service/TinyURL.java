package com.example.pratyush.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Service;

@Service
public class TinyURL {
	String base62 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public String getTinyURL(String url) {
		try {
			//Converting input URL to md5 
			String md5 = md5(url);
			
			//Convert md5 hash to 128 bits
			String bits = asciiToBinary(md5);
			
			//Taking first 43 bits
			String bits43 = bits.substring(0, 43);
			
			//Converting the binary string to decimal 
			long decimal = binaryToDecimal(bits43);
			
			//Getting the Short URL from the decimal value
			String tinyURL = base62Encode(decimal);
			
			return tinyURL;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private String md5(String input) throws NoSuchAlgorithmException {
		MessageDigest md5 = MessageDigest.getInstance("md5");
		byte[] messageDigest = md5.digest(input.getBytes());
		
		BigInteger no = new BigInteger(1, messageDigest);
		
		String hashtext = no.toString(16); 
        while (hashtext.length() < 32) { 
            hashtext = "0" + hashtext; 
        } 
        return hashtext;
	}
	
	private String asciiToBinary(String string) {
		StringBuilder stringBuilder = new StringBuilder();
		for(int i=0;i<string.length();i++) {
			char ch = string.charAt(i);
			switch(ch) {
				case '0':
					stringBuilder.append("0000");
					break;
				case '1':
					stringBuilder.append("0001");
					break;
				case '2':
					stringBuilder.append("0010");
					break;
				case '3':
					stringBuilder.append("0011");
					break;
				case '4':
					stringBuilder.append("0100");
					break;
				case '5':
					stringBuilder.append("0101");
					break;
				case '6':
					stringBuilder.append("0110");
					break;
				case '7':
					stringBuilder.append("0111");
					break;
				case '8':
					stringBuilder.append("1000");
					break;
				case '9':
					stringBuilder.append("1001");
					break;
				case 'a':
					stringBuilder.append("1010");
					break;
				case 'b':
					stringBuilder.append("1011");
					break;
				case 'c':
					stringBuilder.append("1100");
					break;
				case 'd':
					stringBuilder.append("1101");
					break;
				case 'e':
					stringBuilder.append("1110");
					break;
				case 'f':
					stringBuilder.append("1111");
					break;
			}
		}
		
		return stringBuilder.toString();
	}
	
	private long binaryToDecimal(String string) {
		long temp = 0;
		for(int i=0;i<string.length();i++) {
			int ch = (int)string.charAt(i)-48;
			int value = (string.length()-1)-i;
			temp+= (ch * Math.pow(2, value));
		}
		
		return temp;
	}
	
	private String base62Encode(long value) {
	    StringBuilder sb = new StringBuilder();
	    while (value != 0) {
	        sb.append(base62.charAt((int)(value % 62)));
	        value /= 62;
	    }
	    while (sb.length() < 6) {
	        sb.append(0);
	    }
	    return sb.reverse().toString();
	}
}
